import { Output, Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() doMenuToggle = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  toggleMenu($event) {
    this.doMenuToggle.emit($event);
  }

}
