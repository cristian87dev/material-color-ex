import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './layout/main/main.component';
import { View1Component } from './app-routing/view1/view1.component';
import { View2Component } from './app-routing/view2/view2.component';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { MatModule } from './mat/mat.module';
import { HeaderComponent } from './layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    View1Component,
    View2Component,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
